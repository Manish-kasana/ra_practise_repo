package RestAssuredBasics;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import junit.framework.Assert;

public class GetRequest {
	
	
	//@SuppressWarnings("deprecation")
	@Test
	public void restAssuredGet() {
		
		
		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/3");
		System.out.println(response);
		String body = response.asString();
		System.out.println(body);	
		response.prettyPrint();
		System.out.println(response.getStatusCode());
		 String statusCode = response.getStatusLine();
		 System.out.println(statusCode);
		 //Assert.assertEquals("HTTP/1.1 200 OK", statusCode);
		 //pretty print
		 //http codes
		 //run|debug
		 //hard assertion vs soft assertion
		 // object class is super class in java so we can assign any value type to it
			
	}

}
