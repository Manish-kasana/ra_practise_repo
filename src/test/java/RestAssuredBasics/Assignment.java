package RestAssuredBasics;

import java.util.List;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Assignment {
	
	int mResult;
	
	
	
	
		
		private static String token = null;
		
		@Test
		public void createToken() {
			
			token = RestAssured
			.given()
			.header("Content-Type","application/json")
			.body("{\r\n"
					+ "    \"username\" : \"admin\",\r\n"
					+ "    \"password\" : \"password123\"\r\n"
					+ "}")
			.post("https://restful-booker.herokuapp.com/auth")
			.then()
			.extract()
			.jsonPath()
			.get("token");
			
			
			System.out.println(token);
		}
		
		


	@Test(dependsOnMethods = "createToken")
	public void createBooking() {
		
		
		Response response =  RestAssured
		 .given()
		 .header("Content-Type","application/json")
		 .body("{\r\n"
		 		+ "    \"firstname\" : \"Manish\",\r\n"
		 		+ "    \"lastname\" : \"Kasana\",\r\n"
		 		+ "    \"totalprice\" : 111,\r\n"
		 		+ "    \"depositpaid\" : true,\r\n"
		 		+ "    \"bookingdates\" : {\r\n"
		 		+ "        \"checkin\" : \"2018-01-01\",\r\n"
		 		+ "        \"checkout\" : \"2019-01-01\"\r\n"
		 		+ "    },\r\n"
		 		+ "    \"additionalneeds\" : \"All Meals\"\r\n"
		 		+ "}")
				 .post("https://restful-booker.herokuapp.com/booking");
				 
			int bookingId = response.jsonPath().get("bookingid");
			System.out.println(bookingId);
			response .prettyPrint();	
			mResult= bookingId;
			//response.body().
			
			//return bookingID;
			//	System.out.println(jsonPath.getString("firstname"));
			//System.out.println(jsonPath.getString("bookingdates.checkout"));
				
				
				
	}

	
	  @Test(dependsOnMethods = "createBooking") 
	  
	  public void getCreatedBooking()
	  {
		  
		  System.out.println(mResult);
		  
		  Response response = RestAssured
				  .given()
				  .pathParam("id", mResult)
				  .get("https://restful-booker.herokuapp.com/booking/{id}");
	 
	  response .prettyPrint();
	 

}
	  
	  
	  @Test(dependsOnMethods = "createBooking") 
	  public void getAllBookingIds()
	  {
		  
		  System.out.println(mResult);
		  
		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking");
				
		 // response.then()
		  //.assertThat()
	      //.statusCode(200)
	      //.body("$", Matchers.hasItemInArray(elementMatcher)
	      //.body("id",containsString(mResult));
	     // .body("name", equalTo(testMovie.getName()))
	      //.body("synopsis", notNullValue());
		  response.prettyPrint();
		  //response.body("bookingid", Matchers.contains(mResult));
		 // response.bod
		  
		  
		  
		//  RestAssured.get("https://restful-booker.herokuapp.com/booking")
		//  .then()
		 // .body("bookingid", Matchers.contains(mResult));
		  
		  
		  JsonPath jsonPath= response.jsonPath();
		 List l = jsonPath.get("bookingid");
		 
		 for(int i=0;i<l.size();i++){
			    System.out.println(l.get(i));
			    
			    if (l.get(i).equals(mResult))
			    {
			    	System.out.println("my booking id is there in confirmed list and my ID is "+l.get(i));
			    	break;
			    	
			} 
			    
		 }
		 // response.jsonPath()
	   // List l = response.jsonPath().getList(bookingId);
	  }
	  
	  @Test(dependsOnMethods = "getAllBookingIds") 
		public void PutRequest() {
			
		  System.out.println("---==========================================================================-");
		  
		  System.out.println(mResult);
						
			Response response = RestAssured
			.given()
			.log()
			.all()
			.contentType("application/json")
			.header("Cookie", "token="+token)
			.header("Accept", "*/*")
			.pathParam("id", mResult)
			.body("{\r\n"
					+ "    \"firstname\" : \"NewName\",\r\n"
					+ "    \"lastname\" : \"NewLastName\",\r\n"
					+ "    \"totalprice\" : 111,\r\n"
					+ "    \"depositpaid\" : true,\r\n"
					+ "    \"bookingdates\" : {\r\n"
					+ "        \"checkin\" : \"2018-01-01\",\r\n"
					+ "        \"checkout\" : \"2019-01-01\"\r\n"
					+ "    },\r\n"
					+ "    \"additionalneeds\" : \"Breakfast\"\r\n"
					+ "}")
			.put("https://restful-booker.herokuapp.com/booking/{id}");
			
			
		
			response.prettyPrint();
			
			System.out.println("---==========================================================================-");
		
			
			
		}
	  
	  @Test(dependsOnMethods = "PutRequest") 
			public void deleteBooking() {
			  
			  System.out.println(mResult);
				
				
				Response response = RestAssured
				.given()
				.log()
				.all()
				.header("Cookie", "token="+token)
				//.header("Authorisation", "Basic YWRtaW46cGFzc3dvcmQxMjM=")
				.pathParam("id", mResult)
				.body("{\r\n"
				 		+ "    \"firstname\" : \"NewName\",\r\n"
				 		+ "    \"lastname\" : \"Kasana\",\r\n"
				 	   	+ "}")
				.delete("https://restful-booker.herokuapp.com/booking/{id}");
				System.out.println(response.getStatusCode());
				
				response.prettyPrint();
				
				System.out.println("---------------------------------------------------------------------------");
			
				
				
			}
	  
	  
}
