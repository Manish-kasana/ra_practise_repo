package RestAssuredBasics;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import junit.framework.Assert;

public class GetRequest2 {
	
	
	
	@Test
	public void restAssuredGet() {
		
		
		String name = RestAssured.get("https://restful-booker.herokuapp.com/booking/3")
		.then()
		.statusCode(200)
		.statusLine("HTTP/1.1 200 OK")
		.body("firstname", Matchers.equalTo("Susan"))
		.body("bookingdates.checkin", Matchers.equalTo("2017-11-09"))
		.extract()
		.jsonPath()
		.get("firstname");
		
		System.out.println(name);
		
	}

}
