package RestAssuredBasics;

import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class createToken {
	
	private static String token;
	
	@Test
	public void createToken() {
		
		token = RestAssured
		.given()
		.header("Content-Type","application/json")
		.body("{\r\n"
				+ "    \"username\" : \"admin\",\r\n"
				+ "    \"password\" : \"password123\"\r\n"
				+ "}")
		.post("https://restful-booker.herokuapp.com/auth")
		.then()
		.extract()
		.jsonPath()
		.get("token");
		
		
		System.out.println(token);
	}
	
	

}
