package RestAssuredBasics;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;


public class GetRequest4 {
	
	

	@Test
	public void restAssuredGet() {
		
		
		 JsonPath jsonPath = RestAssured.get("https://restful-booker.herokuapp.com/booking/3")
				.then()
				.statusCode(200)
				.statusLine("HTTP/1.1 200 OK")
				.body("firstname", Matchers.equalTo("Susan"))
				.body("bookingdates.checkin", Matchers.equalTo("2017-11-09"))
				.extract()
				.jsonPath();
				
				
				System.out.println(jsonPath.getString("firstname"));
				System.out.println(jsonPath.getString("bookingdates.checkout"));
				
			
				
	}

}
