package RestAssuredBasics;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import junit.framework.Assert;

public class xPatchRequest {
	
	
	
	@Test
	public void xPatchRequest() {
		
		
		Response response = RestAssured
		.given()
		.body("{\r\n"
		 		+ "    \"firstname\" : \"NewName\",\r\n"
		 		+ "    \"lastname\" : \"Kasana\",\r\n"
		 	   	+ "}")
		.patch("https://restful-booker.herokuapp.com/booking/11");
		
		response.prettyPrint();
	
	}

}
