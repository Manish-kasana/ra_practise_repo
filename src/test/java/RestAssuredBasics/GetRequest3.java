package RestAssuredBasics;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;


public class GetRequest3 {
	
	

	@Test
	public void restAssuredGet() {
		
		
		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/3");
		System.out.println(response.jsonPath().get("firstname").toString());
		System.out.println(response.getTime());
		
		Object obj = response.jsonPath().get("firstname");
		System.out.println(obj instanceof String);
		System.out.println(obj instanceof Boolean);
		System.out.println(obj instanceof Integer);
		
		if (obj instanceof String)
			System.out.println(obj.toString());
		else if (obj instanceof Integer)
		{
			int i = (Integer)obj;
		System.out.println(i);
		}
		else
		{
			boolean i = (Boolean)obj;
			System.out.println(i);
		}
			
	}

}
